/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex1 of lab5
 * Task		   : X�y dung lop so phuc voi cac toan tu viet chong
			- constructors
			- constructor copy
			- c�c to�n tu +, -
			- c�c to�n tu so s�nh
			- to�n tu g�n
			- viet chong ph�p << v� >> 
 * Reference   :
 * Date        : 13/5/2015
 */

#include<iostream>

using namespace std;

class ComplexNumber{
	private:
		float realPart, imagPart;
	public:
		ComplexNumber();
		ComplexNumber(float a, float b);
		ComplexNumber(const ComplexNumber &a);
		ComplexNumber operator+ (ComplexNumber a);
		ComplexNumber operator- (ComplexNumber a);
		bool operator== (ComplexNumber a);
		friend ostream &operator <<(ostream &out, const ComplexNumber &a);
		friend istream &operator >>(istream &in,  ComplexNumber &a);
		void print(){
			cout << realPart << "  " << imagPart;
		}
};

ComplexNumber::ComplexNumber(){
	realPart = imagPart = 0;
}
ComplexNumber::ComplexNumber(float a, float b){
	realPart = a;
	imagPart = b;
}
ComplexNumber::ComplexNumber(const ComplexNumber &a){
	realPart = a.realPart;
	imagPart = a.imagPart;
}
ComplexNumber ComplexNumber::operator+ (ComplexNumber a){
	ComplexNumber c;
	c.realPart = a.realPart + realPart;
	c.imagPart = a.imagPart + imagPart;
	return c;
}
ComplexNumber ComplexNumber::operator- (ComplexNumber b){
	ComplexNumber c;
	c.realPart = realPart - b.realPart;
	c.imagPart = imagPart - b.imagPart;
	return c;
}

bool ComplexNumber::operator== (ComplexNumber a){
	if ( a.realPart == realPart && a.imagPart == imagPart)
		return true;
	return false;
}

ostream &operator <<(ostream &out, const ComplexNumber &a){
	if (a.realPart >= 0) cout << a.realPart << " ";
	else out << "-" << 0 - a.realPart << " ";
	if (a.imagPart >= 0) out << "+ " << a.imagPart << "i" << endl;
	else out << "- " << 0-a.imagPart << "i" << endl;
	return out;
}
istream &operator >>(istream &in, ComplexNumber &a){
	cout << "Enter real part and imag part: ";
	in >> a.realPart >> a.imagPart;
	return in;
}

// Now, test those methods above
int main(){
	ComplexNumber a, b, c;
	cout << "Enter the first complex number" << endl;
	cin >> a;
	cout << "Enter the second complex number" << endl;
	cin >> b;
	cout << "Enter the third complex number" << endl;
	cin >> c;
	if ( a + b == c) 
		cout << "a + b = c" << endl;
	else 
		cout << "a + b != c" << endl;
		
	if ( a - b == c) 
		cout << "a + b = c" << endl;
	else 
		cout << "a + b != c" << endl;
				
	return 0;
}
