/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex3 of lab5
 * Task		   :
	X�y dung h�m tong qu�t sap xep mot danh s�ch c�c doi tuong, su dung:
	template 
	
	v� chay 2 v� du doi tuong kh�c nhau

 * Reference   :
 * Date        : 13/5/2015
 */

#include <iostream>
#include <string>

using namespace std;

template <typename T>
void mySwap(T& x, T& y){
	T tmp = x;
	x = y;
	y = tmp;
}
template <typename T>
void sort (T* a, int n) { 
    for ( int i = 0; i < n; i++ )
    	for ( int j = i + 1; j < n; j++)
    		if( a[i] > a[j] ) mySwap(a[i], a[j]); 
} 
int main (){
 	int a[4] = {1,5,4,3};
 	char b[5] = {'g','a','b','z', 'j'};
	sort(a,4);
	sort(b,5);
	cout << a[0] << " " << a[1] << " " << a[2] << " " << a[3] << endl;
	cout << b[0] << " " << b[1] << " " << b[2] << " " << b[3] << " " << b[4] << endl;
   return 0;
}

