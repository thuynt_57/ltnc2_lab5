/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex5 of lab5
 * Task		   :
	X�y dung mot v� du ve su dung t�nh chat da h�nh trong lop, ke thua lop.
	V� du ve b�i to�n t�nh dien t�ch h�nh,
	2 lop ke thua l� h�nh chu nhat v� h�nh vu�ng
 * Reference   :
 * Date        : 13/5/2015
 */

#include <iostream>
#include <string.h>
#include <stdio.h>

using namespace std;

class Hinh{
	public:
		Hinh(){
			soCanh = 0;
		}	
		Hinh(int x){
			soCanh = x;
		}
		void print(){
			cout << soCanh;
		}
		virtual float dienTich() = 0;
	private:
		int soCanh;
};

class HinhChuNhat : public Hinh{
	public:
		HinhChuNhat():Hinh(){
			
		}
		HinhChuNhat(int x):Hinh(x){
		}
		HinhChuNhat(float chieuDai, float chieuRong, int d):Hinh(d){
			this->chieuDai = chieuDai;
			this->chieuRong = chieuRong;
		}
		void print(){
			cout << "Chieu dai: " << chieuDai << "    Chieu rong: " << chieuRong;
		}
		float dienTich(){
			return chieuDai*chieuRong;
		}

	private:
		float chieuDai, chieuRong;
};

class HinhVuong : public Hinh{
	public:
		HinhVuong():Hinh(){
			
		}
		HinhVuong(int x):Hinh(x){
		}
		HinhVuong(float canh, int d):Hinh(d){
			this->canh = canh;
		}
		void print(){
			cout << "Hinh vuong co canh: " << canh;
		}
		float dienTich(){
			return canh*canh;
		}
	private:
		float canh;
};

int main(){
	Hinh *a1 = new HinhVuong(3,4);
	Hinh *a2 = new HinhChuNhat(2,4,4);
	Hinh* a[5] = {a1, a2}; 
	cout << "Dien tich hinh vuong: " << a[0]->dienTich() << endl;
	cout << "Dien tich hinh chu nhat: " << a[1]->dienTich() << endl;
	return 0;
}
