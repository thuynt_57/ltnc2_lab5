/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex2 of lab5
 * Task		   :
	X�y dung lop SinhVien don gian gom hoten, tuoi 
	X�y dung lop danh s�ch li�n ket, moi phan tu trong danh s�ch li�n ket l� mot doi tuong SinhVien
	Viet c�c lop: 
	-	Ch�n th�m 1 sinh vi�n 
	- x�a mot sinh vi�n theo t�n
	- sap xep sinh vi�n theo tuoi
	Viet c�c to�n tu:
	- tao tu
	- huy tu
	- viet chong << >>> (hien thi to�n bo danh s�ch sinh vi�n)

 * Reference   :
 * Date        : 13/5/2015
 */

#include <iostream>
#include <string.h>
#include <stdio.h>

using namespace std;


// -------------------------- Class Student - begin ------------------------
class Student{
	private:
		string name;
		int age;
	public:
	    Student(){
	        name = "";
	        age = 0;
	    }
	    
	    Student( string _name, int _age){
	        name = _name;
	        age = _age;
	    }
	    string getName()const{
	        return name;
	    }
	    ~Student(){
	    	name = "";
	    	age = 0;
	    }
	    int getAge(){
	    	return age;
	    }
	    Student* operator =(Student &std){
	        name = std.getName();
	        age = std.getAge();
	        return this;
	    }
	    bool operator ==( Student &std){
	        if ( name == std.getName() && age == std.getAge() ) {
	            return true;
	        }else{
	            return false;
	        }
	    }
		friend ostream &operator << (ostream &out, const Student &a);
		friend istream &operator >> (istream &in,  Student &a);		
};

ostream &operator << (ostream &out, const Student &a){
	out << "Name: " << a.name << "     Age: " << a.age; 
	return out;
}
istream &operator >>(istream &in, Student &a){
	cout << "Enter the  name: ";
	fflush(stdin);
	getline(in, a.name);
	cout << "Enter the age: ";
	in >> a.age; 
	return in;
}
// ---------------------------- Class Student - end ------------------------

// -----------------------------Struct Node - begin -----------------------
struct Node{
	Student std;
	Node* next;
};
// -----------------------------Struct Node - end ----------------------

// ----------------------------- Class StudentList - begin ------------------
class StudentList{
	private:
		Node* list;
		Node *head = NULL;
		Node *tail = NULL;
	public:
		void addNode();
		void print();
		void delByName();
		void searchByName();
		void sortByName();
		void sortByAge();
		friend ostream &operator << (ostream &out, const StudentList &a);
		friend istream &operator >> (istream &in,  StudentList &a);	
};

Node* createNode(string _name, int _age){
    Node *p = new Node;
    Student data(_name, _age);
    p->std = data;
    p->next = NULL;
    return p;
}

void input(string &name, int &age){
    cout << "Name: ";
    fflush(stdin);
    getline(cin, name);
    cout << "Age: ";
    cin >> age;
}

void StudentList::addNode(){
    string name;
    int age;
    input(name, age);
    Node *p = createNode(name, age);
    if ( head == NULL && tail == NULL) {
        head = p;
        tail = p;
    }else{
        tail->next = p;
        tail = p;
    }
    cin.ignore(1);
}

ostream &operator << (ostream &out, const StudentList &a){
    Node* p = a.head;
    while ( p != NULL) {
        out << p->std << endl;
        p = p->next;
    }
    return out;
}

istream &operator >>(istream &in, StudentList &a){
	int check  = 1;
	while (check){
		a.addNode();
		cout << "1 - continue      0 - quit" << endl;
		cin >> check;
	}
	return in;
}

// use to test operator <<
void StudentList::print(){
    Node* p = head;
    while ( p != NULL) {
        cout << p->std;
        p = p->next;
    }
}

void StudentList::delByName(){
    //Nhap sv can xoa
    Node *p=head;
    string name;
    int age;
    input(name, age);
    Student std(name, age);
 
    if( head->std == std ){
        head=head->next;
        return;
    }
    while( p->next != NULL){
        if( p->next->std == std ){
            p->next = p->next->next;
            if( p->next == NULL){
                tail = p;
            }
            return;
        }
        p = p->next;
    }
    cout<<"Not found\n";
}
void StudentList::searchByName(){
    bool check = true;
    string name;
    Node *p=head;
    cout<<"Enter the student name that u wish to find: ";
    getline(cin,name);
    while (p != NULL) {
        if(p->std.getName() == name){
            check = false;
            cout << p->std.getName() << " - " << p->std.getAge() << endl;
        }
        p = p->next;
    }
    if( check ){
        cout << "Not found\n";
    }
}

void swap(Node *a, Node *b){
    Node c;
    c.std = a->std;
    a->std = b->std;
    b->std = c.std;
}
void StudentList::sortByName(){
    bool check = true;
    while( check ){
        check = false;
        Node *p = head;
        while( p->next != NULL ){
            if( p->std.getName().compare(p->next->std.getName()) > 0 ){
                swap(p, p->next);
                check = true;
            }
            p = p->next;
        }
    }
}

void StudentList::sortByAge(){
    bool check = true;
    while( check ){
        check = false;
        Node *p = head;
        while( p->next != NULL ){
            if( p->std.getAge() > p->next->std.getAge() ){
                swap(p, p->next);
                check = true;
            }
            p = p->next;
        }
    }	
}
//------------------------------------- Class StudentList - end------------

// Now, we turn to test, wish that evething is ok :))
int main(){
	StudentList a;
	int checked=1;
    cout<<"0: Exit\n1: Add\n2: Print\n3: Delete by name\n4: Search by name\n5: Sort by name\n6: Sort by age \n";
    while (checked) {
        cout<<"Enter one of those numbers above: ";
        cin>>checked;
        cin.ignore(1);
        switch (checked) {
            case 1:
                cin >> a;
                break;
            case 2:
                cout << a;
                break;
            case 3:
                a.delByName();
                break;
            case 4:
                a.searchByName();
                break;
            case 5:
                a.sortByName();
                cout << a;
                break;
            case 6:
                a.sortByAge();
                cout << a;
                break;
            default:
                break;
        }
    }
    
	return 0;
}
