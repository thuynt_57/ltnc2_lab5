/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex4 of lab5
 * Task		   : X�y dung lop tong quat cho b�i 2, su dung template, class, DSLK
	v� v� du cho 1 lop doi tuong T cu the, v� du SinhVien
 * Reference   :
 * Date        : 14/5/2015
 */

#include <iostream>
#include <string.h>
#include <stdio.h>

using namespace std;

class Student{
	private:
		string name;
		int age;
	public:
	    Student(){
	        name = "";
	        age = 0;
	    }
	    
	    Student( string _name, int _age){
	        name = _name;
	        age = _age;
	    }
	    string getName()const{
	        return name;
	    }
	    ~Student(){
	    	name = "";
	    	age = 0;
	    }
	    int getAge(){
	    	return age;
	    }
	    Student* operator =(Student &std){
	        name = std.getName();
	        age = std.getAge();
	        return this;
	    }
	    bool operator ==( Student &std){
	        if ( name == std.getName() && age == std.getAge() ) {
	            return true;
	        }else{
	            return false;
	        }
	    }
		friend ostream &operator << (ostream &out, const Student &a);
		friend istream &operator >> (istream &in,  Student &a);		
};

ostream &operator << (ostream &out, const Student &a){
	out << "Name: " << a.name << "     Age: " << a.age; 
	return out;
}
istream &operator >>(istream &in, Student &a){
	cout << "Enter the  name: ";
	fflush(stdin);
	getline(in, a.name);
	cout << "Enter the age: ";
	in >> a.age; 
	return in;
}

template<class T>
struct Node{
	T data;
	Node<T> * next;
};

template<>
struct Node<Student>{
	Student std;
	Node<Student> * next;
};

template<class T>
Node<T>* createNode(T data){
    Node<T> *p = new Node<T>;
    p->data = data;
    p->next = NULL;
    return p;
}

Node<Student>* createNode(string _name, int _age){
    Node<Student> *p = new Node<Student>;
    Student data(_name, _age);
    p->std = data;
    p->next = NULL;
    return p;
}


template<class T>
class LinkedList{
	private:
		Node<T> * list;
		Node<T> *head = NULL;
		Node<T> *tail = NULL;
	public:
		void addNode(T data){
			string name;
	    	Node<T> *p = createNode(data);
    		if ( head == NULL && tail == NULL) {
        		head = p;
        		tail = p;
    		}else{
        		tail->next = p;
        		tail = p;
    		}
		};
};


template<>
class LinkedList<Student>{
	private:
		Node<Student>* list;
		Node<Student> *head = NULL;
		Node<Student> *tail = NULL;
	public:
		void addNode();
		void print();
		void delByName();
		void searchByName();
		void sortByName();
		void sortByAge();
		friend ostream &operator << (ostream &out, const LinkedList<Student> &a);
		friend istream &operator >> (istream &in,  LinkedList<Student> &a);	
};

void input(string &name, int &age){
    cout << "Name: ";
    fflush(stdin);
    getline(cin, name);
    cout << "Age: ";
    cin >> age;
}


void LinkedList<Student>::addNode(){
    string name;
    int age;
    input(name, age);
    Node<Student> *p = createNode(name, age);
    if ( head == NULL && tail == NULL) {
        head = p;
        tail = p;
    }else{
        tail->next = p;
        tail = p;
    }
    cin.ignore(1);
}


ostream &operator << (ostream &out, const LinkedList<Student> &a){
    Node<Student>* p = a.head;
    while ( p != NULL) {
        out << p->std << endl;
        p = p->next;
    }
    return out;
}


istream &operator >>(istream &in, LinkedList<Student> &a){
		a.addNode();
	return in;
}


// use to test operator <<
void LinkedList<Student>::print(){
    Node<Student>* p = head;
    while ( p != NULL) {
        cout << p->std;
        p = p->next;
    }
}


void LinkedList<Student>::delByName(){
    //Nhap sv can xoa
    Node<Student> *p=head;
    string name;
    int age;
    input(name, age);
    Student std(name, age);
 
    if( head->std == std ){
        head=head->next;
        return;
    }
    while( p->next != NULL){
        if( p->next->std == std ){
            p->next = p->next->next;
            if( p->next == NULL){
                tail = p;
            }
            return;
        }
        p = p->next;
    }
    cout<<"Not found\n";
}


void LinkedList<Student>::searchByName(){
    bool check = true;
    string name;
    Node<Student> *p=head;
    cout<<"Enter the student name that u wish to find: ";
    getline(cin,name);
    while (p != NULL) {
        if(p->std.getName() == name){
            check = false;
            cout << p->std.getName() << " - " << p->std.getAge() << endl;
        }
        p = p->next;
    }
    if( check ){
        cout << "Not found\n";
    }
}


void swap(Node<Student> *a, Node<Student> *b){
    Node<Student> c;
    c.std = a->std;
    a->std = b->std;
    b->std = c.std;
}


void LinkedList<Student>::sortByName(){
    bool check = true;
    while( check ){
        check = false;
        Node<Student> *p = head;
        while( p->next != NULL ){
            if( p->std.getName().compare(p->next->std.getName()) > 0 ){
                swap(p, p->next);
                check = true;
            }
            p = p->next;
        }
    }
}

void LinkedList<Student>::sortByAge(){
    bool check = true;
    while( check ){
        check = false;
        Node<Student> *p = head;
        while( p->next != NULL ){
            if( p->std.getAge() > p->next->std.getAge() ){
                swap(p, p->next);
                check = true;
            }
            p = p->next;
        }
    }	
}

int main(){
	LinkedList<Student> a;
	int checked=1;
    cout<<"0: Exit\n1: Add\n2: Print\n3: Delete by name\n4: Search by name\n5: Sort by name\n6: Sort by age \n";
    while (checked) {
        cout<<"Enter one of those numbers above: ";
        cin>>checked;
        cin.ignore(1);
        switch (checked) {
            case 1:
                cin >> a;
                break;
            case 2:
                cout << a;
                break;
            case 3:
                a.delByName();
                break;
            case 4:
                a.searchByName();
                break;
            case 5:
                a.sortByName();
                cout << a;
                break;
            case 6:
                a.sortByAge();
                cout << a;
                break;
            default:
                break;
        }
    }
	
	return 0;
}



