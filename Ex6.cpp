/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex6 of lab5
 * Task		   : Su dung lop vector trong thu vien STL
 * Reference   :
 * Date        : 13/5/2015
 */

#include<iostream>
#include <vector>

using namespace std;

int main (){
	std::vector<int> myVector(2,6);
	int myInt;
	int check = 1; 
	
	while( check ){
		cout << "Add to the end of list:\n";
		cin >> myInt;
		myVector.push_back (myInt);
		cout << "Enter 0 if u want to quit, 1 to continue: ";
		cin >> check;
	}	
	
	cout << "Enter the position u wish to delete:\n";
	cin >> check;
	myVector.erase( myVector.begin() + myInt - 1 );
	
	cout << "Size:"<< int(myVector.size()) << endl;
	
	for (int i = 0; i < int(myVector.size()); i++)
		cout << myVector[i] << " ";
	cout << '\n';
	
	return 0;
}
